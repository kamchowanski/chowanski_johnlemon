﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RandomSpawner : MonoBehaviour
{
    public Transform[] spawnpoints;
    public GameObject[] PickUpPrefabs;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < spawnpoints.Length; i++)
        {
            Transform spawnpoint = spawnpoints[i];
            GameObject pickupPrefab = PickUpPrefabs[(int)(Random.value * PickUpPrefabs.Length)];
            Instantiate(pickupPrefab, spawnpoint.position, pickupPrefab.transform.rotation);


        }

       /*
        int pickupsSpawned = 5;

        while (pickupsSpawned > 0)
        {
            pickupsSpawned--;
            SpawnPower();
        }
        */
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SpawnPower()
    {
        Transform spawnpoint = spawnpoints[(int)(Random.value * spawnpoints.Length)];
        GameObject pickupPrefab = PickUpPrefabs[(int)(Random.value * PickUpPrefabs.Length)];
        Instantiate(pickupPrefab, spawnpoint.position, pickupPrefab.transform.rotation);

    }
}
