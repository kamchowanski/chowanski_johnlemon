﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    GameObject JohnLemon;
    Rigidbody rb;
    public float speed;

    // Start is called before the first frame update
    void Start()
    {
        JohnLemon = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 dirToPlayer = JohnLemon.transform.position - transform.position;
        dirToPlayer = dirToPlayer.normalized;

        rb.AddForce(dirToPlayer * speed);
    }
}
