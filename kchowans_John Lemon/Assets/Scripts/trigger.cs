﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine;

public class trigger : MonoBehaviour
{
    // Destroy everything that enters the trigger
    void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
    }
}